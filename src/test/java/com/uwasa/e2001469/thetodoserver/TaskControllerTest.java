package com.uwasa.e2001469.thetodoserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.uwasa.e2001469.thetodoserver.controller.TaskController;
import com.uwasa.e2001469.thetodoserver.entity.Task;
import com.uwasa.e2001469.thetodoserver.repository.TaskRepository;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Arrays;
import java.util.List;


import static org.hamcrest.Matchers.*;

import static org.mockito.BDDMockito.given;



@WebMvcTest(TaskController.class)
public class TaskControllerTest {
    @Autowired
    ObjectMapper mapper;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private TaskController controller;

    @MockBean
    TaskRepository repository;

    ObjectMapper objectMapper = new ObjectMapper();
    ObjectWriter objectWriter = objectMapper.writer();
    //Testing get
    @Test
    public void getTasksAPI()
            throws Exception {

        Task testi = new Task(0, "Testi", 1, "sub1", "sub2", 3,2);

        List<Task> allTasks = Arrays.asList(testi);

        given(controller.getTasks()).willReturn(allTasks);

        mvc.perform(get("/tasks")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].description", is("Testi")));
    }

    //Testing post
    @Test
    public void createTaskAPI() throws Exception
    {
        mvc.perform( MockMvcRequestBuilders
                        .post("/task")
                        .content(asJsonString(new Task(1, "Testi2", 1, "sub1", "sub2", 3,2)))
                        .contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.description", is("Testi2")));
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
