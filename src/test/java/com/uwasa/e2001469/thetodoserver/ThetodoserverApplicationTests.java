package com.uwasa.e2001469.thetodoserver;

import com.uwasa.e2001469.thetodoserver.controller.TaskController;
import com.uwasa.e2001469.thetodoserver.entity.Task;
import com.uwasa.e2001469.thetodoserver.repository.TaskRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Optional;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;



@SpringBootTest
class ThetodoserverApplicationTests {


    @Autowired
    TaskRepository repository;

    @Autowired
    private TaskController controller;


    @Test
    void contextLoads() {
    }

    @Test
    public void checkThatControllerLoads() throws Exception {
        assertThat(controller).isNotNull();
    }

    //Add item to database, get that item by id and compare
    @Test
    void testAddToDatabase(){

        Task exceptedTask = new Task(1, "Testi", 1, "sub1", "sub2", 3,2);
        repository.save(exceptedTask);

        Optional<Task> resultTask = repository.findById(exceptedTask.getId());
        assertEquals("Testi", resultTask.get().getDescription());

    }



}
