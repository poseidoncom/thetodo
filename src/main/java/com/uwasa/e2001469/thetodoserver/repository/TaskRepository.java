package com.uwasa.e2001469.thetodoserver.repository;

import com.uwasa.e2001469.thetodoserver.entity.Task;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface TaskRepository extends CrudRepository<Task, Integer> {
    List<Task> findAllByOrderByIdAsc();
}
