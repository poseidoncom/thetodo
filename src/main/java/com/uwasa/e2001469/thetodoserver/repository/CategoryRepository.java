package com.uwasa.e2001469.thetodoserver.repository;

import com.uwasa.e2001469.thetodoserver.entity.Category;
import org.springframework.data.repository.CrudRepository;

public interface CategoryRepository extends CrudRepository<Category, Integer> {
}
