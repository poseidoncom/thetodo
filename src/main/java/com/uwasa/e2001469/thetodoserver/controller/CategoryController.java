package com.uwasa.e2001469.thetodoserver.controller;

import com.uwasa.e2001469.thetodoserver.entity.Category;
import com.uwasa.e2001469.thetodoserver.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class CategoryController {
    @Autowired
    CategoryRepository repository;

    @CrossOrigin(origins = {"https://thetodoclient.herokuapp.com/", "http://localhost:3000"})
    @GetMapping("/categories")
    public Iterable<Category> getCategories(){
        return repository.findAll();
    }

    @CrossOrigin(origins = {"https://thetodoclient.herokuapp.com/", "http://localhost:3000"})
    @PostMapping("/category")
    public Category create(@RequestBody Category o){
        return repository.save(o);
    }

    @CrossOrigin(origins = {"https://thetodoclient.herokuapp.com/", "http://localhost:3000"})
    @DeleteMapping("/category/{id}")
    public void deleteCategory(@PathVariable int id){
        repository.deleteById(id);
    }
}
