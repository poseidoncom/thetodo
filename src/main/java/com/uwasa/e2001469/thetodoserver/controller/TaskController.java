package com.uwasa.e2001469.thetodoserver.controller;

import com.uwasa.e2001469.thetodoserver.entity.Task;
import com.uwasa.e2001469.thetodoserver.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class TaskController {
    @Autowired
    TaskRepository repository;

    @CrossOrigin(origins = {"https://thetodoclient.herokuapp.com/", "http://localhost:3000"})
    @GetMapping("/tasks")public Iterable<Task> getTasks(){return repository.findAllByOrderByIdAsc();}

    @CrossOrigin(origins = {"https://thetodoclient.herokuapp.com/", "http://localhost:3000"})
    @GetMapping("/tasks/{id}")public Optional<Task> getById(int id){return repository.findById(id);}

    @CrossOrigin(origins = {"https://thetodoclient.herokuapp.com/", "http://localhost:3000"})
    @PostMapping("/task")
    public ResponseEntity<Task> create(@RequestBody Task o){
        repository.save(o);
        return new ResponseEntity<Task>(o, HttpStatus.CREATED);
    }

    @CrossOrigin(origins = {"https://thetodoclient.herokuapp.com/", "http://localhost:3000"})
    @PutMapping("/task")
    public Task update(@RequestBody Task o){return repository.save(o);}

    @CrossOrigin(origins = {"https://thetodoclient.herokuapp.com/", "http://localhost:3000"})
    @DeleteMapping("/task/{id}")
    public void deleteTask(@PathVariable int id){
        repository.deleteById(id);
    }



}
