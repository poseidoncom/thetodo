package com.uwasa.e2001469.thetodoserver.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Task {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;
    private String description;
    private int categoryId;
    private String subObjective1;
    private String subObjective2;
    private float plannedTime;
    private float usedTime;
    private boolean isDone;

    public Task() {
    }

    public Task(int id, String description, int categoryId, String subObjective1, String subObjective2, float plannedTime, float usedTime) {
        this.id = id;
        this.description = description;
        this.categoryId = categoryId;
        this.subObjective1 = subObjective1;
        this.subObjective2 = subObjective2;
        this.plannedTime = plannedTime;
        this.usedTime = usedTime;
    }

    public boolean isDone() {
        return isDone;
    }

    public void setDone(boolean done) {
        isDone = done;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getSubObjective1() {
        return subObjective1;
    }

    public void setSubObjective1(String subObjective1) {
        this.subObjective1 = subObjective1;
    }

    public String getSubObjective2() {
        return subObjective2;
    }

    public void setSubObjective2(String subObjective2) {
        this.subObjective2 = subObjective2;
    }

    public float getPlannedTime() {
        return plannedTime;
    }

    public void setPlannedTime(float plannedTime) {
        this.plannedTime = plannedTime;
    }

    public float getUsedTime() {
        return usedTime;
    }

    public void setUsedTime(float usedTime) {
        this.usedTime = usedTime;
    }
}
