package com.uwasa.e2001469.thetodoserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ThetodoserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(ThetodoserverApplication.class, args);
    }

}
