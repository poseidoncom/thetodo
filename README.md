# Thetodo server

Spring boot server thetodo webbisovellukselle. Client löytyy [täältä](https://gitlab.com/poseidoncom/thetodoclient).



## Analysis

### Johdanto
Thetodo on tehtävälista sovellus, jonka ensisijaisena tarkoituksena on säästää luontoa korvaamalla ne tuhannet sekalaiset paperin palat, joihin olen joka aamu jo usean vuoden ajan rustannut päivän tehtäviä. Tehtävälista on minulle henkilökohtaisesti paras tapa hahmottaa tekemättömiä tehtäviä ja seurata tavoitteellista edistymistä niin koulussa, töissä kuin harrastuksissakin. Lisäksi olen kiinnostunut hahmottamaan, kuinka monta tuntia todellisuudessa kulutan päivässä kouluhommiin, harrastuksiin ja erilaisiin työprojekteihin. Tehtävälistan digitoiminen helpottaa datan jälkikäsittelyä ja edistymisen seurantaa. Yrittäjänä toimiessa en toistaiseksi ole kellottanut eri projekteihin kuluvaa aikaa, koska tehtävät ovat sekalaisia ja hinnoiteltu kuukausimaksuna. Tätä varten sovellukseen olisi tarkoitus kehittää ajastin, jolla kellotetaan tiettyyn tehtävään kuluvaa aikaa. Sovellukseen pystyy myös lisätä erilaisia projekteja, jotta tietokantaan kerätystä datasta olisi myöhemmin mahdollista koostaa taulukoita tai käyriä siitä, miten paljon aikaa kuluu tietyn projektin kimpussa tai kuinka suunniteltu aika vastaa toteumaa. Periaatteessa tehtävälistan tarkoitus on siis laajeta ominaisuuksilta vastaamaan suppeaa projektinhallintatyökalua.

Tietotyössä olen kokenut henkilökohtaisesti suurena haasteena keskittymisen sirpaloitumisen useaan asiaan kerrallaan. Ajastimen tavoitteena on myös auttaa keskittymään yhteen tehtävään kerrallaan sopivat tauot huomioiden ja sitä kautta työskentelmään tehokkaammin. Tulevaisuuden lisäominaisuutena voisi olla tehtävien aikatauluttaminen kalenteriin niin, että peräkkäin suoritettaisiin mahdollisimman samankaltaisia tehtäviä, mikä helpottaa aivojen kuormitusta kun eri aihealueiden välinen hyppiminen vähentyy.

Sovelluksessa ei vielä ole erilaisia rooleja. On vain yksi käyttäjä, joka pystyy lisäämään tehtävälistaan tehtäviä tarkempine kuvauksineen. Käyttäjä pytyy myös lisäämään uusia kategorioita helpottamaan tehtävien jaottelua. Lisäksi käyttäjä pystyy muokkaamaan jokaista tietokantaan tallennettua tehtävää. Tarkoituksena olisi myös, että käyttäjä pystyy kellottamaan tehtäviin käytetyn ajan ja tallettamaan tiedon tietokantaan.

#### Toiminnalliset vaatimukset

| id | kuvaus | prioriteetti |
| ------ | ------ | ----- |
| F1 | käyttäjä pystyy lisäämään uuden tehtävän joka sisältää osatavoitteita, kategorioan/projektin, sekä suunnitellun keston ja toteutuneen keston | 1  |
| F2 | käyttäjä pystyy muokkaamaan lisättyä tehtävää | 2 |
| F3 | käyttäjä pystyy lisäämään omia kategorioita | 3 |
| F4 | Käyttäjä näkee listan tehtävistä lisätietoineen | 1 |
| F5 | Tehtävän kulunut aika voidaan mitata sovelluksen sisäisellä ajastimella | 4 |


#### Ei-toiminnalliset vaatimukset

| ominaisuudet | kuvaus | 
| ------ | ------ |
| Käytettävyys | Käyttäjä oppii ohjelman käytön 10 minuutissa | 
| Vastausaika | Palvelin vastaa pyytöihin 0,8 sekunnissa | 

#### Projektin aikataulu

![my image](public/Screenshot_2022-05-02_at_11.43.49.png?raw=true "Project Schedule")

### Käyttötapauskaavio

![usecasediagram](public/Screenshot_2022-05-05_at_13.33.27.png?raw=true "Use Case Diagram")

### Testaus

#### [Yksikkötestaus](https://gitlab.com/poseidoncom/thetodo/-/blob/master/src/test/java/com/uwasa/e2001469/thetodoserver/ThetodoserverApplicationTests.java)
- Tehtävän lisääminen tietokantaan
- Tehtävän hakeminen tietokannasta id:n perusteella

#### [Integraatiotestaus](https://gitlab.com/poseidoncom/thetodo/-/blob/master/src/test/java/com/uwasa/e2001469/thetodoserver/TaskControllerTest.java)
- RESTful API GET (tehtävän haku rajapinnasta)
- RESTful API POST (tehtävän lisääminen rajapinnan kautta)

## Design

### Sekvenssikaaviot tehtävien hausta ja tehtävän päivittämisestä
![sekvenssikaaviogettasks](public/Screenshot_2022-05-05_at_13.40.12.png?raw=true "Get tasks")
![sekvenssikaavioupdatetask](public/Screenshot_2022-05-05_at_13.44.12.png?raw=true "Update task")



### Pakkauskaavio

### Luokkakaavio

![serverclassdiagram](public/Screenshot_2022-05-06_at_9.33.38.png?raw=true "Update task")

### Käyttäjätarinat

- Opiskelevana yrittäjänä haluaisin lisätä tehtävälistaan tehtäviä jaoteltuna työ-,koulu-,harrastusprojekteihin.
- Opiskelevana yrittäjänä haluaisin seurata erilaisiin projekteihini käyttämää aikaa.
- Sovelluksen käyttäjänä haluaisin pystyä lisäämään omia projekteja/kategorioita tehtävien jaottelua varten.
